package 
me.plugner.packetinterceptor.packetinterceptor.api.interfaces;

import me.plugner.packetinterceptor.packetinterceptor.api.enums.PacketType;
import me.plugner.packetinterceptor.packetinterceptor.api.handler.PacketHandler;

import net.minecraft.server.v1_8_R3.Packet;

import org.bukkit.entity.Player;
/**
 * @author Plugner
 * @since 1.0 (01/06/2021)
 * @apiNote This is the interface used to send the packet to the developer
 * @see PacketHandler
 * @see me.plugner.packetinterceptor.packetinterceptor.api.PacketAPI
 */
public interface PacketBack 
{
    /**
     * @author Plugner
     * @since 1.0 (01/06/2021)
     * @param player
     * @param packetType
     * @param packet
     * @return If the packet will be cancelled
     */
    boolean onPacketReceived(
        Player player, 
        PacketType packetType, 
        Packet packet
    );
}
