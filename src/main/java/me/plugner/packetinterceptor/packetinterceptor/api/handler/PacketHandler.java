package 
me.plugner.packetinterceptor.packetinterceptor.api.handler;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;

import me.plugner.packetinterceptor.packetinterceptor.api.enums.PacketType;
import me.plugner.packetinterceptor.packetinterceptor.api.interfaces.PacketBack;

import net.minecraft.server.v1_8_R3.Packet;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
/**
 * @author Plugner
 * @since 1.0 (01/06/2021)
 * @apiNote This is the core of the API, it's accessed by reflection from {@link me.plugner.packetinterceptor.packetinterceptor.api.PacketAPI} and give the response to developer, by {@link PacketBack}
 * @see me.plugner.packetinterceptor.packetinterceptor.api.PacketAPI
 * @see CraftPlayer
 * @see PacketBack
 * @see PacketType
 */
public class PacketHandler {

    /**
     * @author Plugner
     * @since 1.0 (01/06/2021)
     * @apiNote This is invoked by {@link me.plugner.packetinterceptor.packetinterceptor.api.PacketAPI}, and again, please don't use directly.
     * @param craftPlayer The player that's will listen the packets
     * @param packetType The type {@link PacketType}
     * @param packetBack Callback to in your code method
     */
    private static void handle(CraftPlayer craftPlayer, PacketType packetType, PacketBack packetBack) {

        // Creating variable ChannelDuplexHandler and defining it by the type. 
        ChannelDuplexHandler channelDuplexHandler;

        // IN = write
        // OUT = read
        if(packetType == PacketType.IN) {

            channelDuplexHandler = new ChannelDuplexHandler() 
            {
                @Override
                public void channelRead
                (
                    ChannelHandlerContext channelHandlerContext, Object packet
                ) 
                throws Exception 
                {
                    // Gets the response of onPacketReceived (if its blocked)
                    boolean packetBlock = packetBack.onPacketReceived(
                        (Player) craftPlayer,
                        packetType,
                        (Packet) packet
                    );

                    // If not blocking the packet, it will execute the super class (ChannelDuplexHandler) method read to continue. 
                    if(!packetBlock) super.channelRead(
                        channelHandlerContext,
                        packet
                    );
                }
            };

        }else if(packetType == PacketType.OUT){
            channelDuplexHandler = new ChannelDuplexHandler() 
            {
                @Override
                public void write
                (
                    ChannelHandlerContext channelHandlerContext, Object packet, ChannelPromise channelPromise
                ) throws Exception 
                {
                    // Gets the response of onPacketReceived (if its blocked)
                    boolean packetBlock = packetBack.onPacketReceived(
                        (Player) craftPlayer,
                        packetType,
                        (Packet) packet
                    );

                    // If not blocking the packet, it will execute the super class (ChannelDuplexHandler) method write to continue. 
                    if(!packetBlock) super.write(
                        channelHandlerContext,
                        packet, 
                        channelPromise
                    );
                }
            };
        }else /* If PacketType is null*/
        {
            return;
        }

        // Creating the pipeline to intercept the communication with the player.
        ChannelPipeline pipeline = craftPlayer.getHandle().playerConnection.networkManager.channel.pipeline();

        // Adding the interceptor before the Minecraft packet_handler what allow us to block the packets.
        pipeline.addBefore("packet_handler", "interceptor_" + craftPlayer.getName(), channelDuplexHandler);


    }
}
