package me.plugner.packetinterceptor.packetinterceptor.api.enums;

import me.plugner.packetinterceptor.packetinterceptor.api.handler.PacketHandler;

/**
 * @author Plugner
 * @since 1.0 (01/06/2021)
 * @apiNote Specifies the type (IN/OUT = READ/WRITE)
 * @see PacketHandler
 * @see me.plugner.packetinterceptor.packetinterceptor.api.PacketAPI
 * @see me.plugner.packetinterceptor.packetinterceptor.api.interfaces.PacketBack
 */
public enum PacketType {
    IN,
    OUT
}
