# PacketListenAPI

**PacketListenAPI** is a little API that makes packet interception and listening too easy, look this example:

    PacketAPI.packetListen(player, PacketType.OUT, new PacketBack() {
        @Override
        public boolean onPacketReceived(Player player, PacketType packetType, Packet packet) {
            Bukkit.getConsoleSender().sendMessage(packet.toString());
            return false /* False = don't block, and True block the packet*/;
        }
    });

Where PacketType is the read/write packet, IN = Read (Client to Server), OUT = Write (Server to Clients).